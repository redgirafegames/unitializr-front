# README #

The project is deployed and can be used at : http://www.unitializr.com

[Backend project](https://bitbucket.org/redgirafegames/unitializr-back/src)

### Unitializr ###

Unitializr is an Open Source initializr for Unity projects.
It allows you to :
- Configure your project name and Unity version
- Configure your project structure with an integrated file editor with code highlighting
- Configure your project packages
- Save templates (only locally for the moment) to reuse configurations.

Inspired by [Spring Initializr](https://start.spring.io/)

This project is unofficial, [Unity](https://unity.com/) has no official connection with this project.

### Technos ###

The project is based on [VueJs](https://vuejs.org/) and [Vuetify](https://vuetifyjs.com/en/) framework.

Third party libraries :
- REST calls are made with [axios](https://github.com/axios/axios)
- Code highlighting is made with [vue-prism-editor](https://www.npmjs.com/package/vue-prism-editor) and [prismjs](https://prismjs.com/)


### Set up ###

##### Environment Variables #####
VUE_APP_UNITIALIZR_SERVER_ADDRESS : Backend url. If the variable is not available, default backend url is set to : http://localhost:8081/.   

##### Run #####
npm install

npm run serve

### Admin view ###
There is a very light admin view accessible by the path /admin

Admin resources are protected by basic auth, to authenticate in the admin view, you need to add user/pass in the url.

Example : http://localhost:8080/#/admin?user=admin&pass=MyPass

### Evolutions Roadmap ###
- Packages manifest configuration. Right now only dependencies can be defined, but the Unity's manifest offers more options.
- Add Undo/redo capability on save/delete actions
- User creation/authentication
- Remote saving project templates
- Remote saving file templates

### Contribution ###

I made this project mainly to learn Go and VueJs, therefore code can be far from perfect.
Contributions and remarks are welcomed. 

### Contact ###

The project is developed and maintained by [RedGirafeGames](http://www.redgirafegames.com)
Contact : redgirafegames@gmail.com

### License ###
[MIT](https://choosealicense.com/licenses/mit/)