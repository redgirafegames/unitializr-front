import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Admin from "../views/Admin";

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: 'Home',
        component: Home,
        meta: {
            title: 'Unitializr by RedGirafeGames',
            metaTags: [
                {
                    name: 'description',
                    content: 'Initializr for Unity projects'
                },
                {
                    property: 'og:description',
                    content: 'Initializr for Unity projects'
                }
            ]
        }
    },
    {
        path: '/admin',
        name: 'Admin',
        component: Admin
    }
]

const router = new VueRouter({
    routes
})

router.beforeEach((to, from, next) => {
    console.log("route to " + to + " from " + from);
    document.title = to.meta.title || 'Unitializr';
    next();
})

export default router
