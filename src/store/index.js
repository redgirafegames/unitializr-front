import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        busy: false,
        loading: false,
        confirmation:
            {
                show: false,
                message: ""
            },
        warning:
            {
                show: false,
                message: ""
            },
        error:
            {
                message: "",
                details: ""
            },
        project:
            {
                name: "MyProject",
                versionName: "",
                versionSuffix: "",
                files: [],
                packages: []
            },
        templates:
            {
                projects: [],
                files: []
            },
        selectedVersion: {
            packages: []
        },
        versions: []
    },
    mutations: {
        showError(state, {message, details}) {
            state.error.message = message;
            state.error.details = details;
        },
        showWarning(state, {message}) {
            state.warning.show = true;
            state.warning.message = message;
        },
        showConfirmation(state, message) {
            state.confirmation.show = true;
            state.confirmation.message = message;
        },
        updateConfirmationState(state, value) {
            state.confirmation.show = value;
        },
        updateWarningState(state, value) {
            state.warning.show = value;
        },
        updateProject(state, project) {
            // Deep duplication of the project object to separate from the project template
            state.project = JSON.parse(JSON.stringify(project));
        },
        updateProjectName(state, name) {
            state.project.name = name;
        },
        updateProjectFiles(state, files) {
            state.project.files = files;
        },
        updateProjectPackages(state, packages) {
            state.project.packages = packages;
        },
        updateSelectedVersion(state, version) {
            if(version != null)
                state.selectedVersion = version;
            else
                state.selectedVersion = { packages : [] }
            state.project.versionName = version != null ? version.name : "";
        },
        updateProjectVersionSuffix(state, suffix) {
            state.project.versionSuffix = suffix;
        },
        updateVersions(state, versions) {
            function compare(versionA, versionB) {

                const nameA = versionA.name.toUpperCase();
                const nameB = versionB.name.toUpperCase();

                let comparison = 0;
                if (nameA > nameB) {
                    comparison = -1;
                } else if (nameA < nameB) {
                    comparison = 1;
                }
                return comparison;
            }
            versions.sort(compare);
            state.versions = versions;
        },
        addProjectTemplates(state, templates) {
            state.templates.projects = state.templates.projects.concat(templates);

            function compare(templateA, templateB) {

                let comparison = 0;
                if (templateA.default) {
                    comparison = -1;
                } else if (templateB.default) {
                    comparison = 1;
                } else if (templateA.user != null && templateA.user.admin){
                    comparison = -1;
                } else if (templateB.user != null && templateB.user.admin){
                    comparison = 1;
                }
                return comparison;
            }
            state.templates.projects.sort(compare);
        },
        initLocalProjectTemplates(state) {
            if (localStorage.getItem('templates.projects')) {
                let localTemplates = JSON.parse(localStorage.getItem('templates.projects'));
                localTemplates.forEach(function (template, index) {
                    this[index].persist = "LOCAL";
                }, localTemplates);

                state.templates.projects = localTemplates;
            }
        },
        saveLocalProjectTemplate(state, template) {
            let existingTemplate = state.templates.projects.find(t => t['name'] === template.name && t['persist'] == "LOCAL");
            if (existingTemplate) {
                let index = state.templates.projects.indexOf(existingTemplate);
                Vue.set(state.templates.projects, index, template)
            } else {
                console.log("Storing new local project template")
                state.templates.projects.push(template);
            }

            let localTemplates = state.templates.projects.filter(t => t.persist === "LOCAL");
            localStorage.setItem('templates.projects', JSON.stringify(localTemplates));
        },
        deleteLocalProjectTemplate(state, template) {
            let index = state.templates.projects.indexOf(template);
            state.templates.projects.splice(index, 1);

            let localTemplates = state.templates.projects.filter(t => t.persist === "LOCAL");
            localStorage.setItem('templates.projects', JSON.stringify(localTemplates));
        },


        addFileTemplates(state, templates) {
            state.templates.files = state.templates.files.concat(templates);

            function compare(templateA, templateB) {

                const nameA = templateA.file.name.toUpperCase();
                const nameB = templateB.file.name.toUpperCase();

                let comparison = 0;
                if (nameA > nameB) {
                    comparison = 1;
                } else if (nameA < nameB) {
                    comparison = -1;
                }
                return comparison;
            }
            state.templates.files.sort(compare);
        },
        initLocalFileTemplates(state) {
            state.templates.files = [];
            if (localStorage.getItem('templates.files')) {
                let localTemplates = JSON.parse(localStorage.getItem('templates.files'));
                localTemplates.forEach(function (template, index) {
                    this[index].persist = "LOCAL";
                }, localTemplates);

                state.templates.files = localTemplates;
            }
        },
        saveLocalFileTemplate(state, template) {
            let existingTemplate = state.templates.files.find(t => t['name'] === template.name);
            if (existingTemplate) {
                let index = state.templates.files.indexOf(existingTemplate);
                Vue.set(state.templates.files, index, template)
            } else {
                console.log("Storing new local file template.")
                state.templates.projects.push(template);
            }

            let localTemplates = state.templates.files.filter(t => t.persist === "LOCAL");
            localStorage.setItem('templates.files', JSON.stringify(localTemplates));
        },
        deleteLocalFileTemplate(state, template) {
            let index = state.templates.files.indexOf(template);
            state.templates.files.splice(index, 1);

            let localTemplates = state.templates.files.filter(t => t.persist === "LOCAL");
            localStorage.setItem('templates.files', JSON.stringify(localTemplates));
        }
    },
    actions: {},
    modules: {}
})
