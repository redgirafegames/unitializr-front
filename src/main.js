import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify'
import axios from 'axios'
import './plugins/mixin'

Vue.config.productionTip = false


let backendBaseUrl = process.env.VUE_APP_UNITIALIZR_SERVER_ADDRESS;
if(backendBaseUrl == null || backendBaseUrl === ""){
  // just a lazy solution to test locally...
  backendBaseUrl = "http://localhost:8081/";
}

const base = axios.create({
  baseURL: backendBaseUrl
});
Vue.prototype.$backend = base;

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
