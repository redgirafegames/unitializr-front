import Vue from 'vue'

let fileExtensions = {
    html: 'mdi-language-html5',
    js: 'mdi-nodejs',
    json: 'mdi-code-json',
    md: 'mdi-language-markdown',
    pdf: 'mdi-file-pdf',
    png: 'mdi-file-image',
    txt: 'mdi-file-document-outline',
    gitignore: 'mdi-file-document-outline',
    xls: 'mdi-file-excel',
    xml: 'mdi-xml',
    unity: 'mdi-unity',
    asset: 'mdi-unity',
    prefab: 'mdi-unity',
    mat: 'mdi-unity',
    cs: 'mdi-language-csharp',
    meta: 'mdi-file-compare',
    unknown: 'mdi-file-remove'
}

// Some utility methods that could be usefull anywhere in the app
Vue.mixin({
    methods: {

        utf8_to_b64(str) {
            return window.btoa(unescape(encodeURIComponent(str)));
        },

        b64_to_utf8(str) {
            return decodeURIComponent(escape(window.atob(str)));
        },

        encodeProjectFiles(project, decode = false) {
            let encodedProject = JSON.parse(JSON.stringify(project));
            let rootFiles = encodedProject.files;
            for (let i = 0; i < rootFiles.length; i++) {
                let file = rootFiles[i];
                this.encodeFile(file, decode);
            }
            return encodedProject;
        },
        encodeFile(file, decode) {
            if (file == null || file == undefined)
                return;

            if (this.isDirectory(file)) {
                if (file.children != null && file.children.length > 0) {
                    for (let i = 0; i < file.children.length; i++) {
                        let childFile = file.children[i];
                        this.encodeFile(childFile, decode)
                    }
                }
            } else {
                file.value = decode ? this.b64_to_utf8(file.value) : this.utf8_to_b64(file.value);
            }
        },

        compareFiles(fileA, fileB) {
            if (this.isDirectory(fileA) && !this.isDirectory(fileB))
                return -1;
            else if (this.isDirectory(fileB) && !this.isDirectory(fileA))
                return 1;

            // Use toUpperCase() to ignore character casing
            const nameA = fileA.name.toUpperCase();
            const nameB = fileB.name.toUpperCase();

            let comparison = 0;
            if (nameA > nameB) {
                comparison = 1;
            } else if (nameA < nameB) {
                comparison = -1;
            }
            return comparison;
        },

        isDirectory(file) {
            if (file == null || file == undefined)
                return false;
            return file.type.toUpperCase() === "DIR";
        },

        setFileUid(file) {
            if (file.uid == undefined)
                file.uid = Math.floor(Math.random() * 10000)

            if (this.isDirectory(file) && file.children != null && file.children.length > 0) {
                for (let i = 0; i < file.children.length; i++) {
                    let f = file.children[i];
                    this.setFileUid(f);
                }
            }
        },

        getFileIcon(file, open) {

            if (file == null || file == undefined)
                return fileExtensions['txt'];

            if (this.isDirectory(file))
                return open ? 'mdi-folder-open' : 'mdi-folder';

            let extension = "";
            if (file.name.indexOf(".") != -1)
                extension = file.name.split(".").pop().toLowerCase();

            if (extension == "")
                return fileExtensions['txt'];

            if (fileExtensions[extension] == undefined)
                return fileExtensions['txt'];

            return fileExtensions[extension];
        }
        ,

        isFileEditable(file) {
            if (file == null || file == undefined)
                return false;

            if (this.isDirectory(file))
                return false;

            if (file.locked)
                return false;

            // The only editable file right now
            if (file.type.toUpperCase() == 'TEXT')
                return true;

            return false;
        }
    }
})