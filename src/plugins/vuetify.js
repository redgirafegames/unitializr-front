import Vue from 'vue';
import Vuetify from 'vuetify/lib';
import Logo from "@/components/Logo";

Vue.use(Vuetify);

export default new Vuetify({
    theme: {
        options: {
            customProperties: true,
        },
        themes: {
            light: {
                primary: '#ff5722',
                secondary: '#f39300',
                accent: '#009688',
                error: '#f44336',
                warning: '#d6ad1f',
                info: '#2196f3',
                success: '#4caf50'
            },
            dark: {
                primary: '#ff5722',
                secondary: '#f18043',
            }
        }
    },
    icons: {
        values: {
            // Specific icon to use custom svg logo
            logo: {
                component: Logo,
            },
        },
    },
});
